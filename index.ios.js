/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

import TabNavigator from 'react-native-tab-navigator';
import Toilet from './js/toilet/toilet.js'

export default class TabNavRN extends Component {

  state = {
    selectedTab: 'toilet'
  }

  renderView() {
    switch (this.state.selectedTab) {
      case 'toilet':
        return <Toilet />
        break;
      case 'read':
        break;
      default:

    }
  }

  render() {
    return(
      <TabNavigator>
          <TabNavigator.Item
          selected={this.state.selectedTab === 'toilet'}
          title="厕所"
          renderIcon={() =>
            <Image source={require('./tab_1_normal.png')} style={{width: 30, height: 30, marginTop: -3, marginBottom: -3}} />
          }
          renderSelectedIcon={() =>
            <Image source={require('./tab_1_selected.png')} style={{width: 30, height: 30, marginTop: -3, marginBottom: -3}} />
          }
          badgeText="19"
          onPress={() =>
            this.setState({ selectedTab: 'toilet' })
          }
        >
        {this.renderView()}
        </TabNavigator.Item>
        <TabNavigator.Item
          selected={this.state.selectedTab === 'read'}
          title="阅读"

          renderIcon={() =>
            <Image source={require('./tab_2_normal.png')} style={{width: 30, height: 30, marginTop: -3, marginBottom: -3}} />
          }
          renderSelectedIcon={() =>
            <Image source={require('./tab_2_selected.png')} style={{width: 30, height: 30, marginTop: -3, marginBottom: -3}} />
          }
          onPress={() =>
            this.setState({ selectedTab: 'read' })
          }
        >
          <View
            style={styles.container}>
            <Text>这是Read页面</Text>
          </View>
        </TabNavigator.Item>
      </TabNavigator>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});

AppRegistry.registerComponent('TabNavRN', () => TabNavRN);
