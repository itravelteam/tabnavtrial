

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  WebView,
  View
} from 'react-native';

export default class WebMap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      source: this.props.source,
      isMargin: this.props.isMargin,
      isShowErrorPage: false,
      isWeather: this.props.isWeather,
      url: this.props.url
    }
  }

  loadError() {
    this.setState({ isShowErrorPage: true })
  }

  render() {
    let url = {uri: this.state.url}
    return (
      <View style={styles.container}>
      {
        this.state.isShowErrorPage?
          <View style={styles.textView}>
            <Text style={styles.text}>请检查网络连接情况</Text>
          </View>
          :
          <WebView
            style={[styles.container,{marginTop: 20}]}
            startInLoadingState={true}
            // onError={this.loadError.bind(this)} // ES5 syntax
            onError={() => {
              this.loadError()
            }} // ES6 syntax
            source={url} />
      }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textView:{
    flex:1,
    justifyContent:'center',
    alignItems: 'center'
  },
  text:{
    fontSize:16,
    fontWeight:'300'
  }
});
